<?php

namespace TeamRock\CommentComponent\Interfaces;

use TeamRock\Interfaces\Comment\Comment As CommentInterface;

/**
 * Interface Comment
 * @package TeamRock\CommentComponent\Interfaces;
 */
interface Comment extends CommentInterface
{
    public function report($reportedBy);
}
