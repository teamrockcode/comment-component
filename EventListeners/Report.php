<?php

namespace TeamRock\CommentComponent\EventListeners;

use TeamRock\CommentComponent\Interfaces\Comment;
use TeamRock\Interfaces\Comment\CommentRepository;

class Report
{
    /** @var CommentRepository $commentRepository */
    protected $commentRepository;

    /**
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * @param Comment $comment
     * @param string $reportedBy The Member reporting this comment
     */
    protected function report(Comment $comment, $reportedBy)
    {
        $comment->report($reportedBy);

        $this->commentRepository->save($comment);
        $this->commentRepository->flush();
    }
}
